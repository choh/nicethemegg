# niceThemeGG

This package contains themes to use with the `ggplot2` package.

The themes aim at being somewhat minimal and clear with a modern look.

It is strongly recommended to use the themes together with the Source Sans Pro
font which can be fetched [here](https://github.com/adobe-fonts/source-sans-pro).
